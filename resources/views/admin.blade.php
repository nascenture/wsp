<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Music Directory</title>

<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
<link href="{{ asset('/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<!-- Fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script>
 var siteUrl = '{{ url("/") }}';
 var token   = '{{csrf_token()}}';
</script>
<!--datepicker-->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type='text/javascript' src='https://ssl.p.jwpcdn.com/6/12/jwplayer.js?ver=4.4.4'></script>
<!--datepicker-->

<!--audio/video-->
<script src="{{asset('/js/jquery.uploadfile.min.js') }}"></script>
<script src="{{asset('/js/jquery.uploadfile.js') }}"></script>
<link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('/css/uploadfile.css') }}" />
<link rel="stylesheet" href="{{ asset('/css/uploadfile.custom.css') }}" />

<!--audio/video-->
  
</head>
<body class="admin-login-body">

	<!-- <nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				
				
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
			
					@if (Auth::guest())
						 
						 
					@else
						
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ ucfirst(Auth::user()->name) }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
			</ul>
			 
				
			</div>
			 
			 
	
	</nav> -->
	
	@if ( Session::has('flash_message') )
		<div class="alert {{ Session::get('flash_type') }}">
			<div>{{ Session::get('flash_message') }}</div>
		</div>
	@endif
	
	@yield('content')
 
</div>
	
	<script>
	$('#confirm-delete').on('show.bs.modal', function(e) {
		var form = $(e.relatedTarget).data('href');
		$('#danger').click(function(){
			$('#'+form).submit();
		});
	})
    </script>
	<style type="text/css">
		.admin-login-body{background-color:#ecf0f5;}
	</style>
</body>
</html>
