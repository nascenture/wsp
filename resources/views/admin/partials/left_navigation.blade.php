<!-- SIDEBAR - START -->
<div class="page-sidebar pagescroll">
    <!-- MAIN MENU - START -->
    <div class="page-sidebar-wrapper" id="main-menu-wrapper"> 
        <ul class='wraplist'>
            <li class='menusection'>
				Main
			</li>
			<li class="@if(Request::segment(2)== 'dashboard') open @endif"> 
			<a href="{{url('/admin/dashboard')}}">
				<i class="fa fa-dashboard"></i>
				<span class="title">Dashboard</span>
			</a>
			</li>
			<li class="@if(Request::segment(2)== 'users') open @endif"> 
				<a href="{{url('/admin/users')}}">
					<i class="fa fa-suitcase"></i>
				<span class="title">Users</span>
				 </a>
			</li>
			<li class="@if(Request::segment(2)== 'settings') open @endif"> 
				<a href="{{url('/admin/settings')}}">
					<i class="fa fa-suitcase"></i>
					<span class="title">Settings</span>
				</a>
			</li>	
        </ul>
    </div>
    <!-- MAIN MENU - END -->
</div>
<!--  SIDEBAR - END -->