@extends('admin.layouts.app')
@section('content')
    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>
    <div class='col-xs-12'>
        <div class="page-title">
            <div class="pull-left">
                <!-- PAGE HEADING TAG - START -->
				<h1 class="title">Edit Settings</h1><!-- PAGE HEADING TAG - END -->
			</div>

             <div class="pull-right hidden-xs">
				<ol class="breadcrumb">
					<li>
						<a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>Home</a>
					</li>
					<li>
						<a href="{{url('/admin/settings')}}">Settings</a>
					</li>
					<li class="active">
						<strong>Edit Setting </strong>
					</li>
				</ol>
               </div>                 
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- MAIN CONTENT AREA STARTS -->
    <div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">{{ucwords( str_replace('-',' ', $settings->first()->type))}}</h2>
            </header>
			
            <div class="content-body">
			 @if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			
			@if(Session::has('flash_message'))
			 <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('flash_message') }} </div>
			@endif
			 
			<form class="form-horizontal" action ="{{URL::to('admin/settings/save')}}" method="POST">
			
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			  
				<div class="form-group">
					<div class="col-sm-3">
						<input type="text" value="Name" class="form-control" readonly>
					</div>
					<div class="col-sm-9">
						<input type="text" class="form-control" value="{{ucwords( str_replace('-',' ', $settings->first()->type))}}" readonly>
					</div>
				</div>
				
				@if($settings)
					<div class="form-group" style="padding-bottom:0px;">
						<div class="col-sm-3">
							<b>Related Settings</b>
						</div>
						<div class="col-sm-9">
							<table class="table" style="margin:0px;">
								<tr class="bg-success">
								<th>Name</th>
								<th>Value</th>
								</tr>
							</table>
						</div>
					</div>
					 
					@foreach($settings as $val)
						
					<div class="form-group">
						<div class="col-sm-3"></div>
						<div class="col-sm-9">
							
							<div class="col-sm-4">
								@if(strlen($val->name) > 30)
									<textarea rows="2" name="name[{{$val->id}}]" class="form-control" readonly>{{$val->name}}</textarea>
								@else
									<input type="text" class="form-control" value="{{$val->name}}" name="name[{{$val->id}}]" readonly>
								@endif	
							</div>
							<div class="col-sm-8">
								<input type="text" name="value[{{$val->id}}]" class="form-control" value="{{$val->value}}">
							</div>
							
						</div>
					</div>	
				
					@endforeach
					<div class="form-group">
						<div class="col-sm-3">
						</div>
						<div class="col-sm-9">
							<div class="col-sm-4">
							</div>
							<div class="col-sm-8">
							<button type="submit" class="btn btn-primary">Save Changes</button>
							</div>
							
						</div>
					</div> 
			    @endif
				
			</form>

			</div>
    </section>
	</div>
@endsection
