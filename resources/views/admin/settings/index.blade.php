@extends('admin.layouts.app')
@section('content')
<!-- START CONTENT -->

<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>

    <div class='col-xs-12'>
        <div class="page-title">

            <div class="pull-left">
                <h1 class="title">Site Settings</h1></div>
                
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- MAIN CONTENT AREA STARTS -->
    


<div class="clearfix"></div>
<div class="col-lg-12">
    <section class="box nobox marginBottom0">
                <div class="content-body">    <div class="row">
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <a href="{{url('admin/settings/type/system')}}">
				<i class='pull-left fa fa-desktop icon-md icon-rounded icon-primary'></i>
                <div class="stats">
                    <h4><strong> </strong></h4>
                    <span>System</span>
					
                </div>
				</a>
            </div>
        </div>
       
        <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <a href="{{url('admin/settings/type/seo')}}">
				<i class='pull-left fa fa-line-chart icon-md icon-rounded icon-purple'></i>
                <div class="stats">
                     <h4><strong> </strong></h4>
                    <span>SEO</span> 
                </div>
				</a>
            </div>
        </div>
		
		 <div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <a href="{{url('admin/settings/type/follow-us')}}">
				<i class='pull-left fa fa-users icon-md icon-rounded icon-accent'></i>
                <div class="stats">
                    <h4><strong> </strong></h4>
                    <span>Follow Us</span>
                </div>
				</a>
            </div>
        </div>
		<div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
                <a href="{{url('admin/settings/type/listing')}}">
				<i class='pull-left fa fa-list icon-md icon-rounded icon-success'></i>
                <div class="stats">
                    <h4><strong> </strong></h4>
                    <span>Listing</span>
                </div>
				</a>
            </div>
        </div>
		<div class="col-lg-3 col-sm-6 col-xs-12">
            <div class="r4_counter db_box">
				  <a href="{{url('admin/settings/type/analytics')}}">
                <i class='pull-left fa fa-area-chart icon-md icon-rounded icon-warning'></i>
                <div class="stats">
                    <h4><strong> </strong></h4>
                    <span>Analytics</span>
                </div>
				</a>
            </div>
        </div>
		
       
    </div> <!-- End .row -->    
    </div>
        </section>
@endsection