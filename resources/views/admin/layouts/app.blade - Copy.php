<!DOCTYPE html>
<html class=" ">
    <head>
        <!-- 
         * @Package: Complete Admin - Responsive Theme
         * @Subpackage: Bootstrap
         * @Version: 2.2
         * This file is part of Complete Admin Theme.
        -->
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>Complete Admin : @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}" type="image/x-icon" />    <!-- Favicon -->
        <link rel="apple-touch-icon-precomposed" href="{{asset('/images/apple-touch-icon-57-precomposed.png')}}">	<!-- For iPhone -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('/images/apple-touch-icon-114-precomposed.png')}}">    <!-- For iPhone 4 Retina display -->
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('/images/apple-touch-icon-72-precomposed.png')}}">    <!-- For iPad -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('/images/apple-touch-icon-144-precomposed.png')}}">    <!-- For iPad Retina display -->




        <!-- CORE CSS FRAMEWORK - START -->
        <link href="{{asset('/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('/fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css"/>
        <link href="{{asset('/css/responsive.css')}}" rel="stylesheet" type="text/css"/>
	    <link href="{{asset('/css/animate.min.css')}}" rel="stylesheet" type="text/css"/>	
    


	   		
    </head>
    <!-- END HEAD -->

		<!-- CORE CSS TEMPLATE - END -->
		<script src="{{ asset('/js/jquery-1.11.2.min.js') }}"></script>
		<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('/js/icheck.min.js') }}"></script>
			
	
	
    <!-- BEGIN BODY -->
    <body class=" "><!-- START TOPBAR -->
<div class='page-topbar '>
    <div class='logo-area'>

    </div>
    <div class='quick-area'>
        <div class='pull-left'>
            <ul class="info-menu left-links list-inline list-unstyled">
                <li class="sidebar-toggle-wrap">
                    <a href="javascript:void(0);" data-toggle="sidebar" class="sidebar_toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>

                <li class="hidden-sm hidden-xs searchform">
                    <form action="ui-search.html" method="post">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input type="text" class="form-control animated fadeIn" placeholder="Search & Enter">
                        </div>
                        <input type='submit' value="">
                    </form>
                </li>
            </ul>
        </div>		
        <div class='pull-right'>
            <ul class="info-menu right-links list-inline list-unstyled">
                <li class="profile">
                    <a href="#" data-toggle="dropdown" class="toggle">                        
						<img src="{{asset('images/admin-profile.jpg')}}" alt="admin-image" class="img-circle img-inline" />
                        <span>Welcome Admin <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu profile animated fadeIn">
                        <li>
                            <a href="{{ url('admin/change-password') }}">
                                <i class="fa fa-wrench"></i>
                                Change Password
                            </a>
                        </li>
                        <li class="last">
                            <a href="{{ url('admin/auth/logout') }}">
                                <i class="fa fa-lock"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
                <!--<li class="chat-toggle-wrapper">
                    <a href="#" data-toggle="chatbar" class="toggle_chat">
                        <i class="fa fa-comments"></i>
                        <span class="badge badge-accent">9</span>
                        <i class="fa fa-times"></i>
                    </a>
                </li>-->
            </ul>			
        </div>		
    </div>

</div>
<!-- END TOPBAR -->
<!-- START CONTAINER -->
<div class="page-container row-fluid container-fluid">

    <!-- SIDEBAR - START -->

<div class="page-sidebar pagescroll">

    <!-- MAIN MENU - START -->
    <div class="page-sidebar-wrapper" id="main-menu-wrapper"> 
        <ul class='wraplist'>	

            <li class='menusection'>Main</li>
                    <li class="open"> 
                    <a href="index.html">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">Dashboard</span>
                        </a>
                    </li>
                    <li class=""> 
                    <a href="{{url('/admin/users')}}">
                    <i class="fa fa-suitcase"></i>
                    <span class="title">Users</span>
                     </a>
                    </li>
                    <li class=""> 
                    <a href="javascript:;">
                    <i class="fa fa-columns"></i>
                    <span class="title">Layouts</span>
                        <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu" >
                            <li>
                            <a class="" href="layout-default.html" >Default Layout</a>
                            </li>
                            <li>
                            <a class="" href="layout-collapsed.html" >Collapsed Menu</a>
                            </li>
                            <li>
                            <a class="" href="layout-chat.html" >Chat Open</a>
                            </li>
                            <li>
                            <a class="" href="layout-boxed.html" >Boxed Layout</a>
                            </li>
                            <li>
                            <a class="" href="layout-boxed-collapsed.html" >Boxed Collapsed Menu</a>
                            </li>
                            <li>
                            <a class="" href="layout-boxed-chat.html" >Boxed Chat Open</a>
                            </li>
                        </ul>
                    </li>
		
        </ul>

    <div class="menustats">    
        <h5>Project Progress</h5>
        <div class="progress">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                </div>
            </div>
        <h5>Target Achieved</h5>
        <div class="progress">
                    <div class="progress-bar progress-bar-accent" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                </div>
            </div>
    </div>

    </div>
    <!-- MAIN MENU - END -->



</div>
<!--  SIDEBAR - END -->
  @yield('content')
		</div>

</div>

</body>
</html>



