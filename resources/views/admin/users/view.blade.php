@extends('admin.layouts.app')
@section('content')

<div class="col-md-12">
  <div class="panel panel-default panel-black">
  <div class="panel-heading">
    <h4>View client detail</h4>
    <a href="{{URL::to('admin/clients')}}" class="pull-right btn btn-success" style="margin-top: -34px;">Back</a>
  </div>
  <div class="panel-body"> @if (count($errors) > 0)
    <div class="alert {{session('flash_type')}}"> <strong>Whoops!</strong> There were some problems with your input.
      <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
      </ul>
    </div>
    @endif
  <?php //echo "<pre>";print_r($client);die;?>
  <form class="form-horizontal" role="form" method="" action="">
  <table class="table table-bordered">
  <tbody>
    <tr>
      <th>First name</th>
      <td>{{ucfirst($client['ClientInfo']->first_name)}}</td>
    </tr>
    <tr>
      <th>Last name</th>
      <td>{{ucfirst($client['ClientInfo']->last_name)}}</td>
    </tr>
    <tr>
      <th>Email</th>
      <td>{{ ($client->email)}}</td>
    </tr>
	   <tr>
      <th>Company</th>
      <td>{{ ($client->company)}}</td>
    </tr>
	<tr>
      <th>Address</th>
      <td>{{ ($client->address)}}</td>
    </tr>
	<tr>
      <th>City</th>
      <td>{{ ($client->city)}}</td>
    </tr>
	 <tr>
      <th>State</th>
      <td>{{ ($client->state)}}</td>
    </tr>
	 <tr>
      <th>Country</th>
      <td>{{ ($client->country)}}</td>
    </tr>
	  <tr>
      <th>Telephone</th>
      <td>{{ ($client->telephone)}}</td>
    </tr>
    </tbody>
  </table>
  </form>
  </div>
  </div>
</div>


@endsection 