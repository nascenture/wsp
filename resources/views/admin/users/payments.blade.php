@extends('admin.layouts.app')
@section('title', 'Clients Listing')
@section('content')

<div class="col-md-12">
  <div class="panel panel-default panel-black">
        <div class="panel-heading">
          <h4>Payment History</h4>
          <!--<a href="users/create" class="pull-right btn btn-success" style="margin-top: -34px;">Add New</a>--></div>
        <div class="panel-body">
		<?php //echo '<pre>'; print_r($payments); die; ?>
		@if(Session::has('message'))
          <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('message') }} </div>
          @endif
          <table class="table orders admin-order users-table">
            <thead class="heading">
              <tr>
                <th>ID</th>
                <th>Payment Type</th>
                <th>Amount</th>
                <th>Date</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1 ; ?>
			@if(count($payments) > 0 )
            @foreach($payments as $key => $payment)
			<?php 
			   $str = date_create($payment->created_at);
			   $date = date_format($str,"Y-m-d"); 
			?>
            <tr>
              <td><?php echo  (($payments->currentPage() - 1 ) * $payments->perPage() ) + $i ?></td>
              <td>{{ $payment->payment_type }}</td>
              <td>@if($payment->amount) ${{ $payment->amount }} @endif </td>
              <td>{{  $date }}</td>
			  <td>{{ $payment->payment_status }}</td>
            </tr>
            <?php $i++; ?>
            @endforeach
			@else
				<tr>
					<td class="text-center" colspan="5">No records</td>
				</tr>
			@endif
              </tbody>
            
          </table>
        </div>
        <?php echo $payments->render(); ?> </div>
       </div>



<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete Customer</h4>
      </div>
      <div class="modal-body"> Are you sure want to delete? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="#" class="btn btn-danger" id="danger">Delete</a> </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirm-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Change Status</h4>
      </div>
      <div class="modal-body"> Are you sure want to change status? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="#" class="btn btn-danger" id="status_danger">Change</a> </div>
    </div>
  </div>
</div>
<style> 
form {    display: inline;} 
</style>
@endsection 