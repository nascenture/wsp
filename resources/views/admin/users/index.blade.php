@extends('admin.layouts.app')
@section('title', 'Users List')
@section('content')
  <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>
    <div class='col-xs-12'>
        <div class="page-title">
            <div class="pull-left">
                <!-- PAGE HEADING TAG - START --><h1 class="title">Users</h1><!-- PAGE HEADING TAG - END -->                            </div>
                   <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li>                            
							<a  href="{{url('/admin/dashboard')}}">
							<i class="fa fa-home"></i>Home</a>
                        </li>
                        <li>
							<a  href="{{url('/admin/users')}}">
                          Users</a>
                        </li>
                        <li class="active">
                            <strong>Users Listing</strong>
                        </li>
						<li style="display:none;">
                            
                        </li>
                    </ol>
                </div>
                               
        </div>
		<div class ="pull-left">
				<form name="search" action="{{URL::to('admin/users')}}" method="get" class="form-inline pull-right">
				<input type="text" name="q" placeholder="search..." class="form-control" value="{{Request::input('q')}}">
				<input type="submit" value="Search" class="btn btn-primary">
			</form>
		</div>
		
		<div class="pull-right" >
			<a  href="{{url('/admin/users/create')}}" class="btn btn-success" >
			<i class="fa fa-plus"></i> Add User </a>
		</div>
    </div>
    <div class="clearfix"></div>
	
	<!-- MAIN CONTENT AREA STARTS -->

<div class="col-lg-12">
				@if(Session::has('flash_message'))
						<div class="alert {{ Session::get('alert-class') }}">
						{{Session::get('flash_message')}}
						</div>
					@endif 
    <section class="box ">
				<header class="panel_header">
                
				
					
                <div class="actions panel_actions pull-right">
                	<a class="box_toggle fa fa-chevron-down"></a>
                    <a class="box_close fa fa-times"></a>
                </div>
            </header>
            <div class="content-body">    <div class="row">
        

		<div class="col-xs-12">
            <!-- ********************************************** -->
            
			<table id="example" class="display table table-hover table-condensed">
                <thead>
                    <tr>
						<th>S.No</th>
                   	 	<th>Name</th>
						<th>Email</th>
						<th>Is Active</th>
						<th>Action</th>
					</tr>
                </thead>
                <tbody>
				<?php 
					$i = (Request::input('page')) ?  (Request::input('page') -1) * $users->perPage() + 1 : 1; 
				?>
				@if(count($users))
					@foreach($users as $key => $value)
						<tr>
						<td>{{ $i++ }}</td>
						<td>{{ ucwords($value->name) }}</td>
						<td>{{ $value->email }}</td>											
						<td>@if($value->status)
                            <span class="btn btn-success cursor-auto">Yes</span>
                            @else
							<span class="btn btn-warning cursor-auto">No</span>
                            @endif
						</td>
						<td>
								
					
						
						 
						<form method="POST" action="{{URL::to('admin/users/destroy')}}/{{$value->id}}" id="delete_{{ $value->id }}" accept-charset="UTF-8">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_method" value="DELETE">						
						<a class="btn btn-primary" href="{{URL::to('admin/users/')}}/edit/{{$value->id}}" id="{{ $value->id }}" role="button"><i class="glyphicon glyphicon-edit"></i> Edit </a>
						<a class="btn my-btn btn-delete btn-danger" data-href="{{$value->id}}" data-toggle="modal" data-target="#confirm-delete" data-delete ='{{$value->name}}' href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>
						</form>
			
                        </td>
						</tr>
					@endforeach
					
				@else
					<tr>
						<td colspan="4">No Users yet</td>
					</tr>
				@endif
				
				
		
				</tbody>
				</table>
				<div class="text-center">
				{{ $users->links() }}
				</div>
            <!-- ********************************************** -->
	     </div>
   	  </div>
    </div>
	
    </section>
	
</div>
<script>
	$(document).ready(function() {
	$('#confirm-delete').on('show.bs.modal', function (e) { 
            var form = $(e.relatedTarget).data('href');
			var data = $(e.relatedTarget).data('delete');
			$('.delete-item').text(data);
            $('#danger').click(function () { 
			    $('#delete_' + form).submit();
            });
        });
	});
	</script>	
<!-- General section box modal start -->
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete Customer</h4>
      </div>
      <div class="modal-body"> Are you sure want to delete <span class='delete-item'></span>? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="#" class="btn btn-danger" id="danger">Delete</a> </div>
    </div>
  </div>
</div>
<!-- MAIN CONTENT AREA ENDS -->

@endsection 