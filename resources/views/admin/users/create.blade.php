@extends('admin.layouts.app')
@section('content')
    <!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper row" style=''>
    <div class='col-xs-12'>
        <div class="page-title">
            <div class="pull-left">
                <!-- PAGE HEADING TAG - START -->
				<h1 class="title">Add User</h1><!-- PAGE HEADING TAG - END -->
			</div>

             <div class="pull-right hidden-xs">
				<ol class="breadcrumb">
					<li>
						<a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>Home</a>
					</li>
					<li>
						<a href="{{url('/admin/users')}}">Users</a>
					</li>
					<li class="active">
						<strong>Add User</strong>
					</li>
				</ol>
               </div>                 
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- MAIN CONTENT AREA STARTS -->
    <div class="col-xs-12">
    <section class="box ">
            <header class="panel_header">
                <h2 class="title pull-left">Basic Info</h2>
            </header>
			
            <div class="content-body">
			 @if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
              @if(Session::has('flash_message'))
			     <div class="alert {{ Session::get('alert-class', 'alert-info') }}"> {{ Session::get('flash_message') }} </div>
			  @endif
    		<form action ="{{URL::to('admin/users/store')}}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
    			<div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-8">
						<div class="form-group">
                            <label class="form-label" for="field-111490">Name <span style="color:red;">*</span></label>
							<span class="desc">e.g. "John Doe"</span>
                            <div class="controls">
                                <input type="text" value="{{old('name')}}" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" >Email <span style="color:red;">*</span></label>
							<span class="desc">e.g. "johndoe@gmail.com"</span>
                            <div class="controls">
                                <input type="text" value="{{old('email')}}" class="form-control" name="email">
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label class="form-label" >Password <span style="color:red;">*</span></label>
							<span class="desc">must be min 6 chars long</span>
                            <div class="controls">
                                <input type="password" value="" class="form-control" name="password">
                            </div>
                        </div>	

                        <div class="form-group">
                            <label class="form-label" >Address</label>
                            <div class="controls">
                                <input type="text" value="{{old('address')}}" class="form-control" name="address">
                            </div>
                        </div> 
						<input type="hidden" name="status" value="1">
						<div class="form-group">
							<input tabindex="5" type="checkbox" id="square-checkbox-2" class="skin-square-green" checked>
							<label class="icheck-label form-label" for="square-checkbox-2">Is Active?</label>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary ">Save</button>
							<a href="{{URL::to('admin/users')}}" class="btn cancel-btn">Cancel</a>
						</div>
						</div>
				</div>
			</form>
			</div>
    </section>
	</div>
@endsection
