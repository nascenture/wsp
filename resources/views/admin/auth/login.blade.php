@extends('admin.layouts.login')

@section('content')
<div class="container-fluid">
	<div class="row margin-top-bottom-4">
		<div class="admin-forms">
			<div class="panel-heading text-center">	
				<img src="{{asset('images/logo-sm.png')}}" class=" margin-bottom-3" alt="logo" />
				<span class="splash-description">Please enter your user information.</span>
			</div>
			
				<div class="panel-body login-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger margin-top-0">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form class="" role="form" method="POST" action="{{ url('admin/auth/dologin') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="col-md-12 col-sm-12 col-xs-12 padding-0 login-form-box">
							<div class="form-group">
								<input type="email" placeholder ="Email Address" class="form-control" name="email" value="{{ old('email') }}" id="email">
							</div>

							<div class="form-group">
								<input type="password" placeholder ="Password" class="form-control" name="password" id="password">
							</div>
							
							<div class="form-group col-md-12 pull-left padding-0 login-tools">
						      	<div class="checkbox checkbox-inline fancy-input">
	                        		<input type="checkbox" id="inlineCheckbox1" value="option1" name="remember">
	                        		<label for="inlineCheckbox1"> Remember Me </label>
	                    		</div>
                    								
							</div>
						</div>
							<div class="form-group login-submit">
								<button type="submit" class="btn btn-block btn-lightpurple">Sign me in</button>
							</div>
						</div>
					</form>
            </div>
		</div>
	</div>
</div>

@endsection



				
