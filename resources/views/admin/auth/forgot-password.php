@extends('layouts.app')
<!-- Main Content -->
@section('content')
<div class="middle clearfix">
  <section class="padding-top-bottom-2">
        <div class="container">
          	   
			<h1 class="text-center text-orange">Forgot Password</h1>
			<h3 class="text-center">Please enter the email address that you used when creating your account</h3>
			 @if (isset($errors) && count($errors) > 0)
			<div class="col-md-8 col-md-offset-2">
	        	<div class="alert alert-danger">
	            	<ul class="padding-left-1">
	                @foreach ($errors->all() as $error)
		              <li >{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
			</div>
		          @endif
		        @if (Illuminate\Support\Facades\Session::has('status'))
				   <p class="alert alert-success">{{ Illuminate\Support\Facades\Session::get('status') }}</p>
				@endif
				<div class="col-md-8 col-md-offset-1 padding-top-bottom-2">
						
								<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
									{{ csrf_field() }}
								
								<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
								<label class="control-label col-md-4" for="email">Email Address</label>
								<div class="col-md-8">
								 <input id="email" type="email" class="form-control" name="email"  value="{{ old('email') }}">
								 
								
								</div>									
								</div>
								<div class="form-group">
								<div class="col-md-8 col-md-offset-4">
									<button type="submit" class="btn btn-orange btn-lg">
										Send Password Reset Link
									</button>
								</div>
								</div>
								</form>
							
						
				</div>
	</div>
</section>
</div>
@endsection

