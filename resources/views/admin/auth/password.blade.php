@extends('admin.layouts.login')
@section('content')
<div class="container-fluid">
	<div class="row margin-top-bottom-4">
		<div class="col-md-8 col-md-offset-2 admin-forms">
			<h1 class="text-center text-purple margin-bottom-2">Forgot Password</h1>
			<h3 class="text-center margin-bottom-4">Please enter the email address that you used when creating your account</h3>
			
				
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('admin/auth/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label"  for="pasword1">Email Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" required value="{{ old('email') }}" id="pasword1">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-md btn-lightpurple">
									Send Password Reset Link
								</button>
							</div>
						</div>
					</form>
				</div>
		
		</div>
	</div>
</div>
@endsection
