<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// route to show the login form
Route::group(array('namespace'=>'Admin', 'prefix' => 'admin','middleware' => 'App\Http\Middleware\AdminMiddleware'),  function()
{
    Route::get('/', array('as' => 'admin', 'uses' => 'AuthController@login'));
	Route::post('auth/dologin', 'AuthController@dologin');
	Route::get('auth/logout', 'AuthController@Logout');
	Route::get('/forgot-password', 'AuthController@forgotPassword');
	Route::get('/change-password', 'AuthController@changePassword');
	Route::post('/store-change-password', 'AuthController@storeChangePassword');
	Route::get('dashboard', 'HomeController@index');
	Route::controller('users','UserController');
	Route::controller('auth','PasswordController');
	Route::get('activate/{id}', 'UsersController@activate');
	
	Route::controller('settings','SettingController');
});