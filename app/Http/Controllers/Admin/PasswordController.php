<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\User;
use DB;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
	
	
	public function getEmail()
	{
	   return view('admin.auth.password');
	}
	
	
	public function postEmail(Request $request)
	{
	   $email_exist=DB::table('users')
		   ->where('users.email',$request->email)
		   ->where('users.role_id',1)
		   ->count();
	   if($email_exist>0)
       {		   
		   
		   $this->validate($request, ['email' => 'required|email']);

		   $response = \Password::sendResetLink($request->only('email'), function ($message) {
			   $message->subject($this->getEmailSubject());

		   });
		   
		   

		   switch ($response) {
			   case \Password::RESET_LINK_SENT:
				   return redirect()->back()->with('status', trans($response));

			   case \Password::INVALID_USER:
				   return redirect()->back()->withErrors(['email' => trans($response)]);
		   }
	   }
	   else
	   {
		    return redirect()->back()->withErrors(['email' =>'Email does not exist.']);
	   }
	}
	
	public function getReset($token = null)
	{
	   
	   if (is_null($token)) {
		   throw new NotFoundHttpException;
	   }

	   return view('admin.auth.reset')->with('token', $token);
	}
	
	
	public function postReset(Request $request)
	{
	   $this->validate($request, [
		   'token' => 'required',
		   'email' => 'required|email',
		   'password' => 'required|confirmed',
	   ]);

	   $credentials = $request->only(
		   'email', 'password', 'password_confirmation', 'token'
	   );

	   $response = \Password::reset($credentials, function ($user, $password) {
		   $this->resetPassword($user, $password);
	   });

	   switch ($response) {
		   case \Password::PASSWORD_RESET:
		   {

			   return redirect('/admin/');
		   }
		   default:
			   return redirect()->back()
						   ->withInput($request->only('email'))
						   ->withErrors(['email' => trans($response)]);
	   }
	}
   
   protected function resetPassword($user, $password)
   {
       
	   
       $user->password = bcrypt($password);

       $user->save();

       //\Auth::login($user);
   }
   
  
   
   
}
