<?php namespace App\Http\Controllers\Admin;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use App\Http\Requests;
	use App\Setting;
	use Session;
	use Validator;
	use DB;

class SettingController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	 
	public function getIndex()
	{  	 
		return view('admin.settings.index');	 
	}
	
	function getType($type){
		
		$settings = Setting::where('type',$type)->orderBy('id')->get();
		 
		if(!$settings->count()){
			return redirect()->back();
		}
		 
		return view('admin.settings.type')->with('settings',$settings);	 
		 
	}
	
	function postSave(Request $request){
		
		$errors = array();
		
		if(is_array($request->value)){
			
			$input = $request->all();
			foreach($request->value as $id => $val){
				
				$setting = Setting::find($id);
				if($val && $setting->field_type){	 
				
					$rules = array('value' => $setting->field_type);
					$validator = Validator::make(array('value' => $val),$rules);
					
					if ($validator->fails()) {
						$error_for = $input['name'][$id];
						$error =  $validator->errors()->all()[0];
						$errors[] = str_replace('value',$error_for, $error);
						continue;
					}
						  
				}
				
				$setting->value = $val;
				$setting->save();
				
			}
			
			if(empty($errors)){
				Session::flash('flash_message', 'Setting saved successfully');
				Session::flash('alert-class', 'alert-success');
			}
		}
		
		return redirect()->back()->withErrors($errors);
		 
		 
	}
	

}
