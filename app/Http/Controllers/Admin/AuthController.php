<?php namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\User;
use Validator;
use Session;

class AuthController extends Controller
{
  
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
     
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
	

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    
	public function login()
	{ 	 
		if(\Auth::user() && \Auth::user()->role == 1){
			return redirect('admin/dashboard');		
		}
		
		return view('admin.auth.login');
	}

  /**
   * Handle a login request.
   *
   * @param  Request  $request
   * @return Response
   */
	public function dologin(Request $request){ 
		
		$this->validate($request, [
		  'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');
		$credentials['role'] = 1;
		
		if (\Auth::attempt($credentials, $request->has('remember'))) {
		  return redirect()->intended('admin/dashboard');
		}

		return redirect('admin/')
			->withInput($request->only('email', 'remember'))
			->withErrors([
			  'email' => 'Invalid credentials.',
			]);
	}

	  /**
	   * Log the user out of the application.
	   *
	   * @return Response
	   */
	public function Logout()
	{	  
		$this->auth->logout();
		\Session::flush();
		return redirect('/admin');
	}
	
	public function changePassword(){
		return view('admin/auth.change-password');
	}
	
	public function storeChangePassword(Request $request){
		
		$input = $request->all();
		$validator = Validator::make($input, [
					'current_password'      => 'required',
	    			'password' 				=> 'required',
	    			'confirm_password' => 'required|same:password',
	    	]);
	    	if ($validator->fails()){
	    		return \Redirect::back()->withErrors($validator)->withInput();
	    		 
	    	}else{
	    		$id = \Auth::id();
	    		$password = \Hash::make($input['password']);
	    		$user = User::find($id);
	    		if (\Hash::check($input['current_password'], $user->password))
       			{
				// The passwords match...
				$user->password = $password;
				$user->save();
				Session::flash('message', 'Password changed successfully.');
				Session::flash('alert-class', 'alert-success');
					
				}else{
					Session::flash('message', 'Current password did not match, Enter correct current password.');
					Session::flash('alert-class', 'alert-danger');
			    }
	    		return \Redirect::back();
	    	}
	}
}
