<?php namespace App\Http\Controllers\Admin;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use App\Http\Requests;
	use App\User;
	use App\Country;
	use Input;
	use Session;
	use Validator;
	use DB;

class UserController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	 
	public function getIndex(Request $request)
	{  
		$query = User::where('role', '!=', 1)->orderBy('id','desc');
		
		if($request->has('q')){
			$str = trim($request->q);
			$query->where('name','like',"%$str%")
				->orWhere('email','like',"%$str%");
		}
		
		$users = $query->paginate(10);
		return view('admin.users.index')->with(compact('users'));	 
	}
	
	function postStore(Request $request) 
	{
		$rules = User::$rules;
		unset($rules['current_password']);
		unset($rules['password_confirmation']);
		
		$input = $request->all();
		$validator = Validator::make($input,$rules); 
		
		if ($validator->fails()) {
			return redirect()->back()->withInput($input)->withErrors($validator->errors());
		}		
				
		$user = new User;
		$input['password'] = bcrypt($request->password);
		$input['role'] = 'user';
		
		User::create($input); 
		Session::flash('flash_message', 'User added successfully');
		Session::flash('alert-class', 'alert-success');
		return redirect('admin/users'); 
	   
	}
	
	public function deleteDestroy(Request $request, $id)
	{
		if($id)
		{
			DB::table('users')->where('id', '=', $id)->delete();			
			User::destroy($id);			
			Session::flash('flash_message', 'User successfully deleted!');
			Session::flash('alert-class', 'alert-success');
		}

		return redirect('admin/users');    
	} 
	
	public function getCreate()

	{	
		return view('admin.users.create');

	}
	
	function getEdit($id)
	{
		$user = User::find($id);
				
        if(is_null($user))
        {
            return redirect('admin/users');
        }
		//echo '<pre>'; print_r($user);
		return view('admin.users.edit')->with(compact('user','user'));
	}
	
	function putUpdate(Request $request, $id ){
	
		$input = $request->all();

		$user =  User::find($id);

		$rules = User::$rules;
		$rules = array(
				'name'=>'required',
				'password' => 'min:6'
			);
			
		if(isset($input['status']) && $input['status']=='on'){
			$input['status'] = 1;
		}else{
			$input['status'] = 0;
		}
		if(empty($input['password'])) {

				unset($rules['password'],$input['password']);

			}
		$validator = Validator::make($input,$rules);

		if ($validator->fails()) {

			return redirect()->back()->withInput($input)->withErrors($validator->errors()); 	

		}		

		if(isset($input['password'])){

		$input['password'] = bcrypt($request->password); 

		}

		$user =  User::find($id);
		
		$user->update($input);

		unset($input['_token']);

		unset($input['_method']);
	
		Session::flash('flash_message', 'User updated successfully.');

		Session::flash('alert-class', 'alert-success');

		return redirect('admin/users');
		
	}

}
