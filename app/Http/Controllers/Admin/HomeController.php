<?php namespace App\Http\Controllers\Admin; 
use App\Http\Controllers\Controller;
use App\Client;
use App\Author;
use App\Job;
use App\User;
use DB;
use App\Helpers\Helper;
use GuzzleHttp\json_decode;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{	 
		
		$users =  User::get();
		$totalUsers = $users->count(); // Total Users
		$activeUsers = $users->where('status',1)->count(); // Active  Users
		$inactiveUsers = $users->where('status',0)->count(); // Inactive  Users
		 
		return view('admin.home')->with(compact
			('totalUsers','activeUsers','inactiveUsers')
		);
	}

}
