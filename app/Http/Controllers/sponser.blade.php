@extends('layouts.app')
@section('content')
<div class="middle">
    <section class="search-bar" id="navigation">
        <div class="container">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 search-section padding-0">
                <!-- <form>
                        <input type="text" name="" id="tags" value="" placeholder="Hotel name, destination or experience" class="search-box">
                        <input type="submit" name="" value="explore" class="submit">
                </form> -->
            </div>
        </div>
    </section>
    <section class="banner-section section padding-top-bottom-8">
        <h2 class="text-center"> Sponsors
            <span><img src="images/heading-bg-white.png" alt="" /></span>
        </h2>
    </section>
    <section class="sponsors section margin-top-bottom-7">
        <div class="container">
            <div class="col-md-12 padding-0">
                <div class="col-md-5 col-sm-6 col-xs-12 sponsors-left">
                    <p> <b>We bring together the finest luxury boutique hoteliers</b>  with key industry insiders. Sponsoring our awards gives you a unique affiliation with unique luxury experiences on an international stage. We're happy to explore novel sponsorship arrangements, tailored to your brand and product to maximise your exposure.
                    </p>
                    <p>
                        If you would like to hear more and discuss a potential sponsorship, please contact:</p>
                    <h4> edward@boutiquehotelawards.com </h4>
                    <aside class="sponser-img">
                        <div class="dwn">
                            <a href="#" class="btn read-btn"> Download </a>
                        </div>
                        <img src="{{asset('images/sponsors-tagged.jpg')}}" alt="" />
                    </aside>
                </div>
                <div class="col-md-7 col-sm-6 col-xs-12 sponsors-right">
                    <div class="col-md-12 sponsor-box">
                        <img src="{{asset('images/JDB-logo.png')}}" alt="" class="" />
                        <p>JDB Fine Hotels & Resorts represents an unsurpassed collection of luxury boutique hotel properties, creating distinctive experiences for the discriminating traveler. Through the JDB Exclusive Offers <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></p>
                    </div>
                    <div class="col-md-12 sponsor-box">
                        <img src="{{asset('images/JDB-logo.png')}}" alt="" class="" />
                        <p>JDB Fine Hotels & Resorts represents an unsurpassed collection of luxury boutique hotel properties, creating distinctive experiences for the discriminating traveler. Through the JDB Exclusive Offers <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></p>
                    </div>
                    <div class="col-md-12 sponsor-box">
                        <img src="{{asset('images/JDB-logo.png')}}" alt="" class="" />
                        <p>JDB Fine Hotels & Resorts represents an unsurpassed collection of luxury boutique hotel properties, creating distinctive experiences for the discriminating traveler. Through the JDB Exclusive Offers <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></p>
                    </div>
                    <div class="col-md-12 sponsor-box">
                        <img src="{{asset('images/JDB-logo.png')}}" alt="" class="" />
                        <p>JDB Fine Hotels & Resorts represents an unsurpassed collection of luxury boutique hotel properties, creating distinctive experiences for the discriminating traveler. Through the JDB Exclusive Offers <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
