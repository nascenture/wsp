<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Best Boutique Hotels - Luxury Boutique Hotels</title>
        <script src="{{ asset('/js/jquery.min.js') }}"></script>
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}">
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/jquery.fancybox.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>
        <div class="wrapper">
            <header class="header">
                <nav id="navig" class="navbar navbar-default black-nav">
                    <div class="container">
                        <div class="col-md-12 header-top text-center"> <a href="#." class="btn btn-default btn-yellow margin-right-8">Awards Gala Dinner</a>
                            <h1 class="logo"><a class="" href="#"> <img src="{{asset('images/logo.png')}}" alt="" /> </a></h1>
                            <a class="mobile-number margin-left-8" href="#"> <i class="fa fa-mobile" aria-hidden="true"></i> +0044 203 151 1688 </a> </div>
                        <div class="navbar-header" id="">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse main-nav" id="myNavbar">
                        <div class="container">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="{{URL::to('/')}}">Home</a></li>
                               <li><a href="{{ URL::to('/boutique-hotels/latest-winners-2016') }}"> Latest Winner</a></li>
                                <li><a href="http://www.peterowen.com/shop/boutique-hotels-selection-2017">The Book</a></li>
                                <li><a href="{{ URL::to('/explore') }}">Explore</a></li>
                                <li><a href="{{ URL::to('/2016-ceremony') }}">Ceremony</a></li>
                                <li><a href="{{url('/awards-process')}}"> Awards Info</a>
                                    <ul>
                                        <li><a href="{{url('/awards-process')}}">AWARDS PROCESS</a></li>
                                        <li><a href="{{url('/nominate')}}">NOMINATE</a></li>
                                        <li><a href="{{url('/winner-area')}}">WINNERS AREA</a> </li>
                                    </ul>
                                </li>
                                <li><a href="{{ URL::to('/about-us') }}">About</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            @if(!Request::segment(1))
            <section class="slider">
                <div class="container">
                    <div class="slide text-center">
                        <h2> Explore the world's finest luxury <br/>
                            boutique hotels</h2>
                    </div>
                </div>
            </section>
            <!-- slider Close --> 
            @endif
            <section class="search-bar" id="navigation">
                <div class="container">
                    <div class="col-md-10 col-md-offset-1 col-sm-12 search-section padding-0">
                        <form>
                            <input type="text" name="" id="search" value="" placeholder="Hotel name, destination or experience" class="search-box">
                            <!-- <input type="text" name="" value="" placeholder="dd/mm/yyyy" class="date"> -->
                            <input type="submit" name="" value="explore" class="submit">
                        </form>
                    </div>
                </div>
            </section>
            <div class="main-content-section"> 
                @yield('content') 
            </div>

            <section class="partner-section section" style="margin-top: 3%;">
                <div class="container">
                    <div class="col-md-12 padding-0 margin-bottom-4">
                        <h2 class="text-center">
                            Official Global Sponsor
                            <span><img src="{{asset('images/heading-bg.png')}}" alt=""></span>
                        </h2>
                        <ul class="list-inline partnar-list text-center">
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2"><a href="http://jdbhotels.com/" target="_blank"><img src="{{asset('images/sponsar-1.png')}}" alt="" /> </a></li>
                        </ul>
                    </div>
                    <div class="col-md-12 padding-0 margin-bottom-4">
                        <h2 class="text-center">
                            Platinum sponsors
                            <span><img src="{{asset('images/heading-bg.png')}}" alt=""></span>
                        </h2>
                        <ul class="list-inline partnar-list text-center">
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2"> 
                                <a href="http://www.laurent-perrier.com/en/" target="_blank"> 
                                    <img src="{{asset('images/sponsar-2.png')}}" alt="" />
                                </a> 
                            </li>
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2"> 
                                <a href="http://www.myprivatevillas.com/" target="_blank"> 
                                    <img src="{{asset('images/sponsar-3.png')}}" alt="" />
                                </a> 
                            </li>
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2"> 
                                <a href="http://www.bestinternational.co.uk/" target="_blank">
                                    <img src="{{asset('images/sponsar-4.png')}}" alt="" />
                                </a> 
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12 padding-0 margin-bottom-4">
                        <h2 class="text-center">
                            Official Media Partner
                            <span><img src="{{asset('images/heading-bg.png')}}" alt=""></span>
                        </h2>
                        <ul class="list-inline partnar-list text-center">
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2"> 
                                <a href="http://www.elitetraveler.com/" target="_blank">
                                    <img src="{{asset('images/partnar-3.png')}}" alt="" /> 
                                </a>
                            </li>
                        </ul>
                    </div>


                    <div class="col-md-12 padding-0 margin-bottom-4">
                        <h2 class="text-center">
                            Partners
                            <span><img src="{{asset('images/heading-bg.png')}}" alt=""></span>
                        </h2>
                        <ul class="list-inline partnar-list text-center">
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2">
                                <a href="https://www.vosswater.com/" target="_blank">
                                    <img src="{{asset('images/partnar-1.png')}}" alt="" /> 
                                </a>
                            </li>
                            <li class="margin-top-bottom-3">
                                <a href="http://www.videoadvisor.co.uk/" target="_blank">
                                    <img src="{{asset('images/partnar-6.png')}}" alt="" />  
                                </a> 
                            </li>
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2">
                                <a href="http://www.narcissusinteriors.com/" target="_blank">
                                    <img src="{{asset('images/partnar-5.png')}}" alt="" /> 
                                </a>
                            </li>
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2">
                                <a href="http://www.epmagazine.co.uk/" target="_blank">
                                    <img src="{{asset('images/partnar-7.png')}}" alt="" /> 
                                </a>
                            </li>
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2"> 
                                <a href="http://en.visoanska.com/" target="_blank">
                                    <img src="{{asset('images/partnar-4.png')}}" alt="" />
                                </a>
                            </li>
                            <li class="margin-top-bottom-3 sm-margin-top-bottom-2">
                                <a href="https://eu.marcolini.com/" target="_blank">
                                    <img src="{{asset('images/partnar-2.png')}}" alt="" />  
                                </a> 
                            </li>
                        </ul>
                    </div>
                    @if(Request::path() != 'sponsors') 
                        <div class="col-md-12 text-center padding-bottom-7">
                             <a href="{{url('/sponsors')}}" class="btn btn-yellow"> Find Out More</a>
                        </div>
                    @endif
                </div>
            </section>



            <!-- footer section -->
            <div class="footer padding-top-5">
                <div class="container">
                    <div class="col-md-3 col-sm-4">
                        <h4>Help</h4>
                        <a href="{{ URL::to('/about-us') }}">About us</a>
                        <a href="{{url('/contact-us')}}">Contact us</a>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <h4>Our top countries</h4>
                        <a href="#.">United Kingdom</a>
                        <a href="#.">Morocco</a>
                        <a href="#.">Spain</a>
                        <a href="#.">Italy</a>
                        <a href="#.">France</a>
                        <a href="#.">Portugal</a>
                        <a href="#.">Greece</a>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <h4>Boutique hotel collections</h4>
                        <a href="#.">Barcelona</a> <a href="#.">London</a> <a href="#.">Paris</a> <a href="#.">Rome</a> <a href="#.">New York</a> <a href="#.">Santorini</a> <a href="#.">Marrakech</a> 
                    </div>


                    <div class="col-md-3 col-sm-12 subscribe">
                        <h4>Lets stay in touch</h4>
                        <ul class="list-inline social-box">
                            <li> <a href="https://www.facebook.com/WorldBoutiqueHotelAwards"  title="Facebook" target="_blank" class="facebook"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
                            <li> <a href="https://www.instagram.com/boutiquehotelawards/" class="insta" title="Instagram" target="_blank"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
                            <li> <a href="https://twitter.com/BoutiqueAwards" title="Twitter" target="_blank" class="twitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
                            <li> <a href="https://uk.linkedin.com/company/world-boutique-hotel-awards-ltd" title="LinkedIn" target="_blank" class="linkedin"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
                        </ul>

                        
                        @if (session()->has('flash_message'))
                            <div class="alert {{session('flash_type')}}">
                               {{ session('flash_message') }}
                            </div>
                         @endif
                      <form class="" role="form" method="POST" action="{{URL::to('newslatter-store')}}" id='newslatter'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <h4>Sign up to our newsletter</h4>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                     
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                           
                            <input type="text" name="name" value="{{old('name')}}" class="form-control 
                            margin-bottom-1" placeholder="Name">
                            <input type="Email" name="email" value="{{old('email')}}" class="form-control margin-bottom-1" placeholder="Email">
                            <input type="submit" name="" value="Subscribe" class="btn btn-subscribe">


                        </form>
                    </div>
                </div>
                <div class="footer-bottom padding-top-bottom-1 margin-top-4">
                    <div class="container">
                        <nav class="list-inline footer-nav">
                            <li class="active"><a href="{{URL::to('/')}}">Home</a></li>
                            <li><a href="#"> Latest Winner</a></li>
                            <li><a href="http://www.peterowen.com/shop/boutique-hotels-selection-2017">The Book</a></li>
                            <li><a href="{{ URL::to('/explore') }}">Explore</a></li>
                            <li><a href="{{ URL::to('/2016-ceremony') }}">Ceremony</a></li>
                            <li><a href="#">Awards Info</a></li>
                            <li><a href="{{ URL::to('/about-us') }}">About</a></li>
                        </nav>
                        <p class="pull-right">© {{date('Y')}} - World Boutique Hotel Awards </p>
                    </div>
                </div>
            </div>
            <!-- Footer Close --><!-- Footer Close --> 
        </div>
        <!-- wrapper Close --> 
        <script src="{{ asset('/js/bootstrap.js') }}"></script> 
        <script src="{{ asset('/js/jquery-ui.js') }}"></script> 
        <script src="{{ asset('/js/jquery.fancybox.js') }}"></script> 
        <script src="{{ asset('/js/jquery.priceformat.js') }}"></script> 
        @if(!Request::segment(1)) 
        <script type="text/javascript">
$(window).scroll(function () {
    if ($(window).scrollTop() >= $('.extraordinary-hotels').position().top) {
        $('#myNavbar').css({'position': 'fixed', 'top': '0', 'width': '100%', 'transition': 'all 1s ease;'});
        $('.search-bar').css('top', $('#myNavbar').height());
        $('.search-bar').addClass('fixed-header');
    } else {
        $('#myNavbar').attr('style', '');
        $('.search-bar').attr('style', '');
        $('.search-bar').removeClass('fixed-header');
    }
});
        </script> 
        @endif 
        <script type="text/javascript">

            $(function () {
                $.widget("custom.catcomplete", $.ui.autocomplete, {
                    _create: function () {
                        this._super();
                        this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
                    },
                    _renderMenu: function (ul, items) {
                        var that = this,
                                currentCategory = "";
                        $.each(items, function (index, item) {
                            var li;
                            if (item.category != currentCategory) {
                                ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                                currentCategory = item.category;
                            }
                            li = that._renderItemData(ul, item);
                            if (item.category) {
                                li.attr("aria-label", item.category + " : " + item.label);
                            }
                        });
                    }
                });
                $("#search").catcomplete({
                    minLength: 2,
                    source: function (request, response) {
                        var $this = $(this);
                        var $element = $(this.element);
                        var jqXHR = $element.data('jqXHR');
                        if (jqXHR) {
                            jqXHR.abort();
                        }
                        var term = request.term;
                        $element.data("jqXHR", $.getJSON("search", request, function (data, status, xhr) {
                            var matching = $.grep(data, function (value) {
                                var name = value.value;
                                var url = value.url;
                                var category = value.category;
                                return name || url || category;
                            });
                            response(matching);
                        }));
                    }
                });

                $.ui.autocomplete.prototype._renderItem = function (ul, item) {
                    var str = "<a href=" + item.url + ">" + item.label + "</a>";
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append(str)
                            .appendTo(ul);
                };

            });

            $(document).ready(function () {
                $('.fancybox').fancybox();
                $('.fancybox-buttons').fancybox({
                    openEffect: 'none',
                    closeEffect: 'none',
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    helpers: {
                        title: {
                            type: 'inside'
                        },
                        buttons: {}
                    },
                    afterLoad: function () {
                        this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
                    }
                });
            });
        </script>
        <style type="text/css">

        </style>
    </body>
