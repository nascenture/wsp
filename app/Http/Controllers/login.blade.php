<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tragetwriters | Admin Login</title>

  <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
	 <link href="{{ asset('/css/admin.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
	<script> var siteUrl = '{{ url("/") }}'; </script>
</head>
<body id="app-layout" ng-app="main-App">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand">
                  <img src="{{asset('images/logo.png')}}" alt="logo" />
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                   
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    
                     
					        
	                    
                </ul>
            </div>
        </div>
    </nav>
 	<div class="container">
 	 <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default border-0 margin-top-bottom-4">
						@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
			<h2 class="page-title text-center"> Login</h2>
                <div class="panel-body margin-top-bottom-2">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('admin/auth/dologin') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				  <div class="form-group">
				  <label for="email" class="col-md-4 control-label" for="mail">E-mail Address</label>

                     <div class="col-md-6">
						<input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" id="mail">
					</div>
				  </div>
				  <div class="form-group">
				  <label for="email" class="col-md-4 control-label" for="password">Password</label>

                      <div class="col-md-6">
							<input type="password" class="form-control" placeholder="Password" name="password" id="password">
					</div>
				  </div>
				   <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
 
                            </div>
                            
                                
                        </div>
				 
				</form>
				</div>
				</div>
			 </div>
			</div>
		</div>
			 

	</body>
</html>		