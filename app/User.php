<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','remember_token','status','role','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	 public static $rules = array(
		'name'					=> 'required',
		'email'					=> 'required|email|unique:users',
		'password'            	=> 'required|min:6',
		'current_password'      => 'required|min:6'
		
    );
}
